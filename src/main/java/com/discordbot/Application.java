package com.discordbot;

import com.discordbot.properties.DiscordBotPropertiesReader;
import com.discordbot.service.DiscordClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sx.blah.discord.util.DiscordException;

import java.io.IOException;

@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    private DiscordBotPropertiesReader discordBotPropertiesReader;

    @Autowired
    private DiscordClientService discordClientService;

    @Override
    public void run(String... args) throws IOException, DiscordException {
        String token;
        if (args.length > 0) {
            token = args[0];
        } else {
            token = discordBotPropertiesReader.getToken();
        }
        discordClientService.initialize(token);
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
