package com.discordbot.event;

import sx.blah.discord.api.events.Event;
import sx.blah.discord.handle.obj.IMessage;

public class CommandExecutionEvent extends Event {

    private IMessage message;
    private String command;
    private String[] args;

    public CommandExecutionEvent(IMessage message, String command, String[] args) {
        if (message == null) throw new IllegalArgumentException("message");
        if (command == null) throw new IllegalArgumentException("command");

        this.message = message;
        this.command = command;
        if (args == null) {
            this.args = new String[0];
        } else {
            this.args = args;
        }
    }

    public IMessage getMessage() {
        return message;
    }

    public String getCommand() {
        return command;
    }

    public String[] getArgs() {
        return args;
    }
}
