package com.discordbot.event;

import org.apache.commons.lang3.ArrayUtils;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public class CommandExecutionEventProvider {

    public static final String KEY = "!";

    @EventSubscriber
    public void onMessageEvent(MessageReceivedEvent event) throws IOException, UnsupportedAudioFileException, DiscordException {
        IMessage message = event.getMessage();
        if (!message.getContent().startsWith(KEY)) return;

        String command = message.getContent().substring(KEY.length());
        String[] args = null;
        if (command.contains(" ")) {
            String[] splitCommand = command.split(" ");
            command = splitCommand[0];
            args = ArrayUtils.subarray(splitCommand, 1, splitCommand.length);
        }

        event.getClient().getDispatcher().dispatch(new CommandExecutionEvent(message, command, args));
    }
}
