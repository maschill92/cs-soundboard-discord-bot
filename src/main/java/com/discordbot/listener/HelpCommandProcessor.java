package com.discordbot.listener;

import com.discordbot.event.CommandExecutionEvent;
import com.discordbot.event.CommandExecutionEventProvider;
import com.discordbot.listener.audio.Sound;
import com.discordbot.listener.audio.SoundCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;

public class HelpCommandProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(HelpCommandProcessor.class);

    @EventSubscriber
    public void onEvent(CommandExecutionEvent event) {
        if (!isHelpCommand(event.getCommand())) {
            return;
        }

        IGuild guild = event.getMessage().getGuild();
        IChannel channel = event.getMessage().getChannel();
        if (channel == null) {
            LOGGER.debug("Can't send help command to guild {}. It doesn't have any text channels", guild.getName());
            return;
        }

        String message = generateHelpMessage();

        try {
            channel.sendMessage(message);
        } catch (MissingPermissionsException e) {
            LOGGER.debug("Can't send help command to guild {}. I don't have permissions.", guild.getName(), e);
        } catch (DiscordException e) {
            LOGGER.debug("Error sending message to guild {}.", guild.getName(), e);
        }
    }

    private boolean isHelpCommand(String command) {
        return command.equals("csbothelp");
    }

    private String generateHelpMessage() {
        StringBuilder builder = new StringBuilder();
        builder.append("```\n");
        builder.append("<command>: [<possible args>]\n");
        SoundCollection.SOUND_COLLECTIONS.forEach((name, soundCollection) -> {
            builder.append(CommandExecutionEventProvider.KEY).append(name).append(": [");

            for (int i = 0; i < soundCollection.getSounds().size(); i++) {
                Sound sound = soundCollection.getSounds().get(i);
                builder.append(sound.getName());
                if (i < soundCollection.getSounds().size() - 1) {
                    builder.append(", ");
                }
            }

            builder.append("]\n");
        });


        builder.append("\n```\n");
        builder.append("Bot invite link: https://discordapp.com/oauth2/authorize?&client_id=173106799526805504&scope=bot&permissions=0");
        return builder.toString();
    }
}
