package com.discordbot.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;

public class ReadyEventListener {
    private static Logger LOGGER = LoggerFactory.getLogger(ReadyEventListener.class);

    @EventSubscriber
    public void onReady(ReadyEvent event) {
        event.getClient().online("!csbothelp");
    }
}
