package com.discordbot.listener.audio;

import com.discordbot.event.CommandExecutionEvent;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.util.audio.AudioPlayer;
import sx.blah.discord.util.audio.events.TrackFinishEvent;
import sx.blah.discord.util.audio.events.TrackStartEvent;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class PlayCsSoundCommandProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlayCsSoundCommandProcessor.class);

    @EventSubscriber
    public void onEvent(CommandExecutionEvent event) {
        if (!isAudioCommand(event.getCommand())) {
            return;
        }

        IMessage message = event.getMessage();
        IUser author = message.getAuthor();
        IGuild guild = message.getGuild();

        String command = event.getCommand();

        IVoiceChannel usersChannelInGuild = author.getVoiceStateForGuild(guild).getChannel();
        if (usersChannelInGuild == null) {
            LOGGER.info("User {} is trying to run command {}, but couldn't be found in a guild {} channel.",
                    author.getName(), command, guild.getName());
            return;
        }


        SoundCollection soundCollection = SoundCollection.SOUND_COLLECTIONS.get(command);
        if (soundCollection == null) {
            LOGGER.info("Invalid sound requested. Command was {}", command);
            return;
        }

        Sound soundToPlay;
        if (event.getArgs().length > 0) {
            String desiredSoundName = event.getArgs()[0];
            soundToPlay = soundCollection.getSounds().stream()
                    .filter(sound -> sound.getName().equals(desiredSoundName))
                    .findFirst().orElse(null);
            if (soundToPlay == null) {
                LOGGER.info("Could not find sound with command '{}' and arg '{}'", command, desiredSoundName, author.getName());
                return;
            }
        } else {
            soundToPlay = soundCollection.randomSound();
        }
        AudioPlayer audioPlayerForGuild = AudioPlayer.getAudioPlayerForGuild(guild);
        usersChannelInGuild.join();
        try {
            audioPlayerForGuild.queue(soundToPlay.getFile());
        } catch (IOException | UnsupportedAudioFileException e) {
            ExceptionUtils.wrapAndThrow(e);
        }
    }

    private boolean isAudioCommand(String command) {
        return SoundCollection.SOUND_COLLECTIONS.containsKey(command);
    }

    @EventSubscriber
    public void onTrackFinishEvent(TrackFinishEvent event) {
        IGuild audioGuild = event.getPlayer().getGuild();
        List<IVoiceChannel> connectedChannelsInAudioPlayerGuild = event.getClient().getConnectedVoiceChannels().stream()
                .filter(iVoiceChannel -> audioGuild.equals(iVoiceChannel.getGuild()))
                .collect(Collectors.toList());
        connectedChannelsInAudioPlayerGuild.forEach(ch -> {
            if (ch.isConnected()) {
                ch.leave();
            }
        });
    }
}
