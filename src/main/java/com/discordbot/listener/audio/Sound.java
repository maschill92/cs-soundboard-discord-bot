package com.discordbot.listener.audio;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.File;
import java.net.URISyntaxException;

public class Sound {

    public static final Sound DAZED_COACH = new Sound("/audio/dazed_coach.mp3", "coach", 1500);
    public static final Sound DAZED_INSANE_0 = new Sound("/audio/dazed_insane_0.mp3", "crazy", 2000);
    public static final Sound DAZED_INSANE_1 = new Sound("/audio/dazed_insane_1.mp3", "insane1", 1500);
    public static final Sound DAZED_INSANE_2 = new Sound("/audio/dazed_insane_2.mp3", "insane2", 1500);
    public static final Sound DAZED_INSANE_3 = new Sound("/audio/dazed_insane_3.mp3", "fak", 3500);

    public static final Sound DAZED_GLAWKS_1 = new Sound("/audio/dazed_glocks_1.mp3", "lose", 2000);
    public static final Sound DAZED_GLAWKS_2 = new Sound("/audio/dazed_glocks_2.mp3", "fak!", 2200);
    public static final Sound DAZED_GLAWKS_3 = new Sound("/audio/dazed_glocks_3.mp3", "glawks!", 1400);
    public static final Sound DAZED_GLAWKS_5 = new Sound("/audio/dazed_glocks_5.mp3", "insane", 3000);
    public static final Sound DAZED_GLAWKS_7 = new Sound("/audio/dazed_glocks_7.mp3", "glawks", 1300);
    public static final Sound DAZED_GLAWKS_8 = new Sound("/audio/dazed_glocks_8.mp3", "fak", 1900);
    public static final Sound DAZED_GLAWKS_13 = new Sound("/audio/dazed_glocks_13.mp3", "chkn", 1500);
    public static final Sound DAZED_GLAWKS_14 = new Sound("/audio/dazed_glocks_14.mp3", "pro", 7000);
    public static final Sound DAZED_MANAGER_1 = new Sound("/audio/dazed_manager_1.mp3", "manager", 1700);
    public static final Sound DAZED_MANAGER_2 = new Sound("/audio/dazed_manager_2.mp3", "fak", 2500);
    public static final Sound DAZED_MANAGER_3 = new Sound("/audio/dazed_manager_3.mp3", "man", 2300);

    public static final Sound HYPEMAN_NOISE = new Sound("/audio/hypeman_give_ us_some_noise_6.mp3", "noise", 3700);

    public static final Sound HYPEMAN_CANS_IN_THE_AIR_2 = new Sound("/audio/hypeman_cans_in_the_air_2.mp3", "cans", 2300);
    public static final Sound HYPEMAN_PUT_YOUR_CANS_IN_THE_AIR_3 = new Sound("/audio/hypeman_put_your_cans_in_the_air_3.mp3", "air", 2300);

    public static final Sound HYPEMAN_READY = new Sound("/audio/hypeman_are_you_ready_4.mp3", "ready", 5500);
    public static final Sound HYPEMAN_READY_2 = new Sound("/audio/hypeman_are_you_ready_5.mp3", "readiah", 5500);

    public static final Sound STEEL_INSANE = new Sound("/audio/steel_thats_insane.mp3", "steel", 1700);
    public static final Sound STEEL_FUCK = new Sound("/audio/steel_FUUCK.mp3", "FAK", 2000);
    public static final Sound STEEL_GOD_FUCKING_DAMMIT = new Sound("/audio/steel_god_fucking_dammit.mp3", "DAMMIT", 2000);
    public static final Sound STEEL_WHAT_1 = new Sound("/audio/steel_what_1.mp3", "wat", 1200);
    public static final Sound STEEL_WHAT_2 = new Sound("/audio/steel_what_2.mp3", "wat?", 1200);
    public static final Sound STEEL_WHAT_3 = new Sound("/audio/steel_what_3.mp3", "wat?!", 1200);
    public static final Sound STEEL_WHAT_4 = new Sound("/audio/steel_what_4.mp3", "what?", 1200);
    public static final Sound STEEL_WHAT_5 = new Sound("/audio/steel_what_5.mp3", "long", 1700);
    public static final Sound STEEL_WHAT_ARE_YOU_DOING = new Sound("/audio/steel_what_are_you_doing.mp3", "doing", 1900);

    private String clipName;
    private String name;
    private long clipLengthMs;

    private Sound(String clipName, String name, int clipLengthMs) {
        if (clipName == null) throw new IllegalArgumentException("clipName");
        if (name == null) throw new IllegalArgumentException("name");
        if (clipLengthMs <= 0) throw new IllegalArgumentException("clipLengthMs");

        this.clipName = clipName;
        this.name = name;
        this.clipLengthMs = clipLengthMs;

    }

    public long getClipLengthMs() {
        return clipLengthMs;
    }

    public String getName() {
        return name;
    }

    public File getFile() {
        try {
            return new File(this.getClass().getResource(clipName).toURI());
        } catch (URISyntaxException e) {
            ExceptionUtils.wrapAndThrow(e);
        }
        return null;
    }

}
