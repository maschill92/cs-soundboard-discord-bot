package com.discordbot.listener.audio;

import java.util.*;

public class SoundCollection {

    public static final Map<String, SoundCollection> SOUND_COLLECTIONS = new HashMap<>();
    private static final Random random = new Random();

    static {
        SOUND_COLLECTIONS.put(
                "manager",
                new SoundCollection(
                        new ArrayList<>(Arrays.asList(new Sound[]{
                                Sound.DAZED_COACH,
                                Sound.DAZED_MANAGER_1,
                                Sound.DAZED_MANAGER_2,
                                Sound.DAZED_MANAGER_3
                        }))));
        SOUND_COLLECTIONS.put(
                "insane",
                new SoundCollection(
                        new ArrayList<>(Arrays.asList(new Sound[]{
                                Sound.DAZED_INSANE_0,
                                Sound.DAZED_INSANE_1,
                                Sound.DAZED_INSANE_2,
                                Sound.DAZED_INSANE_3,
                                Sound.STEEL_INSANE
                        }))));
        SOUND_COLLECTIONS.put(
                "glawks",
                new SoundCollection(
                        new ArrayList<>(Arrays.asList(new Sound[]{
                                Sound.DAZED_GLAWKS_1,
                                Sound.DAZED_GLAWKS_2,
                                Sound.DAZED_GLAWKS_3,
                                Sound.DAZED_GLAWKS_5,
                                Sound.DAZED_GLAWKS_7,
                                Sound.DAZED_GLAWKS_8,
                                Sound.DAZED_GLAWKS_13,
                                Sound.DAZED_GLAWKS_14
                        }))));
        SOUND_COLLECTIONS.put(
                "wat",
                new SoundCollection(
                        new ArrayList<>(Arrays.asList(new Sound[]{
                                Sound.STEEL_WHAT_1,
                                Sound.STEEL_WHAT_2,
                                Sound.STEEL_WHAT_3,
                                Sound.STEEL_WHAT_4,
                                Sound.STEEL_WHAT_5,
                                Sound.STEEL_WHAT_ARE_YOU_DOING
                        }))));
        SOUND_COLLECTIONS.put(
                "fak",
                new SoundCollection(
                        new ArrayList<>(Arrays.asList(new Sound[]{
                                Sound.STEEL_FUCK,
                                Sound.STEEL_GOD_FUCKING_DAMMIT
                        }))));
        SOUND_COLLECTIONS.put(
                "ready",
                new SoundCollection(
                        new ArrayList<>(Arrays.asList(new Sound[]{
                                Sound.HYPEMAN_READY,
                                Sound.HYPEMAN_READY_2
                        }))));
        SOUND_COLLECTIONS.put(
                "cans",
                new SoundCollection(
                        new ArrayList<>(Arrays.asList(new Sound[]{
                                Sound.HYPEMAN_NOISE,
                                Sound.HYPEMAN_CANS_IN_THE_AIR_2,
                                Sound.HYPEMAN_PUT_YOUR_CANS_IN_THE_AIR_3
                        }))));
    }

    private List<Sound> sounds;

    private SoundCollection(List<Sound> sounds) {
        if (sounds == null) throw new IllegalArgumentException("sounds");

        this.sounds = sounds;
    }

    public List<Sound> getSounds() {
        return sounds;
    }

    public Sound randomSound() {
        return this.sounds.get(random.nextInt(this.sounds.size()));
    }
}
