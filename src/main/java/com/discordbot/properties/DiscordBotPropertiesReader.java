package com.discordbot.properties;

import java.io.IOException;

public interface DiscordBotPropertiesReader {
    String getToken() throws IOException;
}
