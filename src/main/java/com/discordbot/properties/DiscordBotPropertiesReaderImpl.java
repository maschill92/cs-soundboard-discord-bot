package com.discordbot.properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Component
@Scope
public class DiscordBotPropertiesReaderImpl implements DiscordBotPropertiesReader {

    private static Logger logger = LoggerFactory.getLogger(DiscordBotPropertiesReaderImpl.class);

    private Properties properties;

    @Override
    public String getToken() throws IOException {
        return getProperties().getProperty("token");
    }

    private Properties getProperties() throws IOException {
        if (properties != null) {
            return properties;
        }
        InputStream propertiesStream = getClass().getClassLoader().getResourceAsStream("discord-bot.properties");
        if (propertiesStream == null) {
            throw new RuntimeException("Could not find discord-bot.properties file.");
        }
        Properties properties = new Properties();
        try {
            properties.load(propertiesStream);
            this.properties = properties;
            return properties;
        } catch (IOException e) {
            logger.error("Couldn't load discord-bot.properties as a resource.");
            throw e;
        }
    }
}
