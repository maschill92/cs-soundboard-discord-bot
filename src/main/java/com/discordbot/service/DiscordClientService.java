package com.discordbot.service;

import sx.blah.discord.util.DiscordException;

public interface DiscordClientService {
    void initialize(String token) throws DiscordException;
}
