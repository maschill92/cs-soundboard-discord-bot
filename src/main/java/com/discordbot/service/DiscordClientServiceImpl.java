package com.discordbot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.util.DiscordException;

@Component
public class DiscordClientServiceImpl implements DiscordClientService {

    private final EventSubscriberRegistrar eventSubscriberRegistrar;

    @Autowired
    public DiscordClientServiceImpl(EventSubscriberRegistrar eventSubscriberRegistrar) {
        this.eventSubscriberRegistrar = eventSubscriberRegistrar;
    }

    @Override
    public void initialize(String token) throws DiscordException {
        IDiscordClient discordClient = new ClientBuilder().withToken(token).withRecommendedShardCount().build();
        eventSubscriberRegistrar.registerListeners(discordClient);

        // Only login after all events are registered otherwise some may be missed.
        discordClient.login();
    }
}
