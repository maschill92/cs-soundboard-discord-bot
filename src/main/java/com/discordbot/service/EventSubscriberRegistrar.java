package com.discordbot.service;

import sx.blah.discord.api.IDiscordClient;

public interface EventSubscriberRegistrar {
    void registerListeners(IDiscordClient discordClient);
}
