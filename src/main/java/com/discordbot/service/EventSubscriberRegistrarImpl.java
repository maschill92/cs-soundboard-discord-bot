package com.discordbot.service;

import com.discordbot.event.CommandExecutionEventProvider;
import com.discordbot.listener.HelpCommandProcessor;
import com.discordbot.listener.ReadyEventListener;
import com.discordbot.listener.audio.PlayCsSoundCommandProcessor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import sx.blah.discord.api.IDiscordClient;

@Component
@Scope
public class EventSubscriberRegistrarImpl implements EventSubscriberRegistrar {
    @Override
    public void registerListeners(IDiscordClient discordClient) {
        discordClient.getDispatcher()
                .registerListeners(
                        new ReadyEventListener(),
                        new CommandExecutionEventProvider(),
                        new HelpCommandProcessor(),
                        new PlayCsSoundCommandProcessor());
    }
}
